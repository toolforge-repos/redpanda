#!/usr/bin/python
# -*- coding: utf-8 -*-

# (C) shizhao, 2020
#
# Distributed under the terms of the MIT license.
#

from __future__ import absolute_import, division, unicode_literals

import base64
import hashlib
import io
import re
import json

import pywikibot
from pywikibot import config, textlib
from pywikibot.comms.http import fetch
from pywikibot.specialbots import UploadRobot
from pywikibot.tools import PY2

try:
    import flickrapi  # see: http://stuvel.eu/projects/flickrapi
except ImportError as e:
    flickrapi = e
from scripts import flickrripper

if not PY2:
    from urllib.parse import urlencode
else:
    from urllib import urlencode

import datetime

flickr = flickrapi.FlickrAPI(
    config.flickr['api_key'], config.flickr['api_secret'])

# see https://www.flickr.com/services/api/flickr.interestingness.getList.html
def interestingness(flickr, date, per_page='1'):
    while True:
        try:
            json_bytes = flickr.do_flickr_call('flickr.interestingness.getList',
                                               date=date,
                                               format='json',
                                               extras='license,url_s',
                                               per_page=per_page)
            json_str = json_bytes.decode('UTF-8')
            return json.loads(json_str)
        except flickrapi.exceptions.FlickrError:
            pywikibot.output('Flickr api problem, sleeping')
            pywikibot.sleep(30)

def isAllowedLicense(license):
    """
    Fork flickrripper.py

    Check if the image contains the right license.

    """
    if flickrripper.flickr_allowed_license[int(license)]:
        return True
    else:
        return False

dt = datetime.datetime.now() - datetime.timedelta(days=3)
date = dt.strftime('%Y-%m-%d')
#pywikibot.output('Flickr Explore: ' + date)

# begin in 2004-01-07
for arg in pywikibot.handleArgs():
    if arg.startswith('-start:'):
        date = arg[7:]
    if arg.startswith('-n:'):
        daterange = arg[3:]       
site = pywikibot.Site('commons', 'commons')
datepage = pywikibot.Page(site, u'User:Red panda bot/status')
date = datepage.text
out = []
for i in range(0, int(daterange)):
    dt = datetime.datetime.strptime(date,'%Y-%m-%d') + datetime.timedelta(days=1)
    date = dt.strftime('%Y-%m-%d')
    pywikibot.output('Flickr Explore: ' + date)
    i+=1

    jsondata = {}
    jsondata['date'] = date
    #print(jsondata)

    flickr = flickrapi.FlickrAPI(
        config.flickr['api_key'], config.flickr['api_secret'])

    data = interestingness(flickr, date, per_page='500')

    #print(data)
    photos = []
    i=0
    for photo in data['photos']['photo']:
        license = photo['license']
        if isAllowedLicense(license):
            photodata = {}
            photodata['id'] = photo['id']
            try:
                photodata['url_s'] = photo['url_s']
            except:
                photodata['url_s'] = None
            photodata['license'] = photo['license']
            photodata['title'] = photo['title']
            #photodata['description'] = photo['description']
            photos.append(photodata)
            #print(photos)
            i+=1

    jsondata['photos'] =  photos
    #print(jsondata)
    out.append(jsondata)
    print("Total photos: " + str(i) )
        
outjson = json.dumps(out)
#print(outjson)

with open('./public_html/flickrout.json', 'w') as f:
    f.write(outjson)
print("Go to: https://tools.wmflabs.org/redpanda/")
