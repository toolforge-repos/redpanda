#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
A tool to transfer Flickr Explore photos to Wikimedia Commons.

Fork flickrripper.py

"""
#
# (C) Multichill, 2009
# (C) Pywikibot team, 2009-2020
# (C) shizhao, 2020
#
# Distributed under the terms of the MIT license.
#

from __future__ import absolute_import, division, unicode_literals

import base64
import hashlib
import io
import re
import json

import pywikibot
from pywikibot import config, textlib
from pywikibot.comms.http import fetch
from pywikibot.specialbots import UploadRobot
from pywikibot.tools import PY2

try:
    import flickrapi  # see: http://stuvel.eu/projects/flickrapi
except ImportError as e:
    flickrapi = e
from scripts import flickrripper

if not PY2:
    from urllib.parse import urlencode
else:
    from urllib import urlencode
import xml

flickr = flickrapi.FlickrAPI(
    config.flickr['api_key'], config.flickr['api_secret'])

photo_id = '8443'
(photoInfo, photoSizes) = flickrripper.getPhoto(flickr, photo_id)
#xml.etree.ElementTree.dump(photoSizes)
fileformat = photoInfo.find('photo').attrib['originalformat']
xml.etree.ElementTree.dump(photoInfo)
print(fileformat)

def PhotoSize(photoSizes):
    for size in photoSizes.find('sizes').findall('size'):
        height = size.attrib['height']
        width = size.attrib['width']
    size = int(height) * int(width)
    return size

print(PhotoSize(photoSizes))
photoUrl = flickrripper.getPhotoUrl(photoSizes)
print(photoUrl)
